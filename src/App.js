import React from 'react';
import './app.css';

function App() {
  return (
    <div className="app">
      <header className="app-header">
        <p>Crowdfund app</p>
      </header>
    </div>
  );
}

export default App;
